let random = Math.round(Math.random()*100); // Gerando números até 99

console.log(random); // Ver qual é o número antes de entrar na condicional

function fizzbuzz(random){
    if (random % 3 == 0 && random % 5 == 0){
        console.log("fizzbuzz");
    } else if (random % 3 == 0){
        console.log("fizz");
    } else if (random % 5 == 0) {
        console.log("buzz");
    } else {
        console.log(`O número foi: ${random}`)
    }
}

fizzbuzz(random);


