let grades = [5, 4, 3]
 
function calcGrade(grades){
    let sum = 0
    for(i of grades){
        sum += i
    }
    
    let average = sum / 3
    
    if(average >= 6){
        console.log(`Aprovado com média de ${average}.`)
    } else {
        console.log(`Reprovado com média de ${average}.`)
    }
}

calcGrade(grades);
